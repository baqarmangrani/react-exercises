import React from "react";
import Main from "./exercises/1-birthday-reminder/main";

function App() {
  return (
    <div className="container">
      <Main />
    </div>
  );
}

export default App;
